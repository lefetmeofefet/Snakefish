const config = require("../config/config");
const Snake = require("./snake");


module.exports = class Field {
    constructor(width, height) {
        this.width = width;
        this.height = height;
        this.snakes = [];
        this.apples = [];
        this.createApple()
    }

    serialize() {
        return {
            w: this.width,
            h: this.height,
            apples: this.apples,
            snakes: this.snakes.map(snake => snake.serialize()),
            frameTime: 1000.0 / config.snakeFps,
            type: "field"
        }
    }

    getSnakesStatistics() {
        return this.snakes.map((snake) => {
            return Object.assign(snake.statistics, {
                name: snake.name
            })
        })
    }

    update() {
        if (Math.random() > config.apples.chanceToSpawn) {
            this.createApple();
        }

        for (let snake of this.snakes) {
            snake.update(this.width, this.height);
            for (let apple of this.apples) {
                if (apple.x === snake.head.x && apple.y === snake.head.y) {
                    snake.eatApple(apple);
                    let i = this.apples.indexOf(apple);
                    if (i !== -1) {
                        this.apples.splice(i, 1);
                    }
                    while (this.apples.length < config.apples.minimumApples) {
                        this.createApple();
                    }
                }
            }
        }

        let snakesToKill = [];
        for (let snake of this.snakes) {
            for (let obstacleSnake of this.snakes) {
                for (let node of obstacleSnake.nodes) {
                    if (snake.head.x === node.x && snake.head.y === node.y && snake.head !== node) {
                        snakesToKill.push(snake);
                    }
                }
            }
        }

        for (let snake of snakesToKill) {
            snake.die();
            this.randomizeSnake(snake);
        }
    }

    removeSnake(snake) {
        let i = this.snakes.indexOf(snake);
        if(i !== -1) {
            this.snakes.splice(i, 1);
        }
    }

    addSnake(socketAddress, snakeName) {
        let newSnake = new Snake(socketAddress, snakeName);
        this.randomizeSnake(newSnake);
        this.snakes.push(newSnake);
        return newSnake;
    }

    randomizeSnake(snake){
        if (!snake.color){
            snake.color = getRandomInt(0, 0xffffff);
        }
        let x = getRandomInt(0, this.width - 1);
        let y = getRandomInt(0, this.height - 1);
        snake.nodes = [{
            x: x,
            y: y
        }];

        let dir;
        switch(getRandomInt(0, 3)) {
            case 0:
                dir = "l";
                break;
            case 1:
                dir = "r";
                break;
            case 2:
                dir = "u";
                break;
            case 3:
                dir = "d";
                break;
        }
        snake.dir = dir;
    }

    createApple() {
        let newApple;
        while (true) {
            newApple = {
                x: getRandomInt(0, this.width),
                y: getRandomInt(0, this.height)
            };

            let success = true;
            for (let snake of this.snakes) {
                for (let node of snake.nodes) {
                    if (node.x === newApple.x && node.y === newApple.y) {
                        success = false
                    }
                }
            }
            for (let apple of this.apples) {
                if (apple.x === newApple.x && apple.y === newApple.y) {
                    success = false;
                }
            }
            if (success) {
                this.apples.push(newApple);
                break;
            }
        }
    }
};


function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}