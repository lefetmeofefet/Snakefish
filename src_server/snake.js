module.exports = class Snake {
    get head() {
        return this.nodes[this.nodes.length - 1];
    }

    constructor(socketAddress, name) {
        this.socketAddress = socketAddress;
        this.lastKeepaliveFrame = undefined;
        this.color = null;
        this.name = name;

        this.ateApple = false;
        this.nextDir = undefined;
        this.doubleTurn = undefined;
        this.dir = "r";
        this.nodes = [];
        this.statistics = {
            maxLength: 0,
            score: 0,
            deaths: 0
        }
    }

    serialize() {
        return {
            nodes: this.nodes,
            head: this.head,
            color: this.color,
            socketAddress: this.socketAddress
        }
    }

    update(width, height) {
        if (this.nextDir !== undefined){
            this.dir = this.nextDir;

            if (this.doubleTurn !== undefined){
                this.nextDir = this.doubleTurn;
                this.doubleTurn = undefined;
            }
            else{
                this.nextDir = undefined;
            }
        }

        let newHead;

        if (this.dir === "l"){
            newHead = {
                x: this.head.x - 1,
                y: this.head.y
            }
        }
        else if (this.dir === "r"){
            newHead = {
                x: this.head.x + 1,
                y: this.head.y
            }
        }
        else if (this.dir === "u"){
            newHead = {
                x: this.head.x,
                y: this.head.y - 1
            }
        }
        else if (this.dir === "d"){
            newHead = {
                x: this.head.x,
                y: this.head.y + 1
            }
        }

        if (newHead.x > width - 1){
            newHead.x = 0
        }
        if (newHead.y > height - 1){
            newHead.y = 0
        }
        if (newHead.x < 0){
            newHead.x = width - 1
        }
        if (newHead.y < 0){
            newHead.y = height - 1
        }

        this.nodes.push(newHead);

        if (!this.ateApple){
            this.nodes.shift();
        }
        else{
            this.ateApple = false;
        }

        // Update statistics
        if (this.nodes.length > this.statistics.maxLength) {
            this.statistics.maxLength = this.nodes.length;
        }
    }

    eatApple(){
        this.ateApple = true;
    }

    die() {
        this.statistics.deaths += 1;
    }

    leftPressed() {
        if (this.dir !== "r" && this.dir !== "l"){
            this.nextDir = "l"
        }
        else if (this.nextDir !== undefined && (this.dir === "l" || this.dir === "r")){
            this.doubleTurn = "l"
        }
    }

    rightPressed() {
        if (this.dir !== "l" && this.dir !== "r"){
            this.nextDir = "r"
        }
        else if (this.nextDir !== undefined && (this.dir === "l" || this.dir === "r")){
            this.doubleTurn = "r"
        }
    }

    upPressed() {
        if (this.dir !== "d" && this.dir !== "u"){
            this.nextDir = "u"
        }
        else if (this.nextDir !== undefined && (this.dir === "u" || this.dir === "d")){
            this.doubleTurn = "u"
        }
    }

    downPressed() {
        if (this.dir !== "u" && this.dir !== "d"){
            this.nextDir = "d"
        }
        else if (this.nextDir !== undefined && (this.dir === "u" || this.dir === "d")){
            this.doubleTurn = "d"
        }
    }
};