const dgram = require('dgram');
const Config = require("../config/config");


module.exports = class UdpServer {
    static get PORT () {
        return Config.port;
    }
    static get ADDRESS () {
        return '127.0.0.1';
    }
    static get MAX_AUTHENTICATING_USERS () {
        return 10
    }

    constructor(address = UdpServer.ADDRESS, port = UdpServer.PORT) {
        this.server = dgram.createSocket('udp4');
        this.messageQueue = [];
        this.authenticatingClients = {};
        this.port = port;
        this.address = address;
    }

    startListening() {
        this.server.on('listening', () => {
            let address = this.server.address();
            let port = address.port;
            let family = address.family;
            let ipaddr = address.address;
            console.log('Server is listening at port ' + port);
            console.log('Server ip :' + ipaddr);
            console.log('Server is IP4/IP6 : ' + family);
        });

        this.server.on('message', (message, remote) => {
            let parsedMessage = JSON.parse(message);
            let fullMessage = {
                message: parsedMessage,
                socket: {
                    address: remote.address,
                    port: remote.port
                }
            };

            if (parsedMessage.type === "auth") {
                this.challengeClient(fullMessage.socket);
            }
            else if (parsedMessage.type === "challenge") {
                this.handleChallengeResponse(fullMessage);
            }
            else{
                this.messageQueue.push(fullMessage)
            }
        });

        this.server.on('error',(error) => {
            console.log('Error: ' + error);
            this.server.close();
        });

        this.server.on('close', () => {
            console.log('Socket is closed !');
        });

        this.server.bind(this.port, '0.0.0.0');
    }

    challengeClient(socketAddress) {
        let challengeString = "random string";
        let addressString = socketAddress.address;
        if (Object.keys.length > UdpServer.MAX_AUTHENTICATING_USERS) {
            delete this.authenticatingClients[Object.keys(this.authenticatingClients)[0]];
        }
        this.authenticatingClients[addressString] = challengeString;
        this.sendMessage(JSON.stringify({
            challenge: challengeString
        }), socketAddress);
        console.log("Challenging new client with: ", challengeString)
    }

    handleChallengeResponse(message) {
        console.log("Client returned challenge response: ", message);
        console.log("Authenticating clients list: ", this.authenticatingClients);
        if (message.message.challenge === this.authenticatingClients[message.socket.address]) {
            console.log("Client accepted! message: ", message);
            this.messageQueue.push(message)
        }
        delete this.authenticatingClients[message.socket.address];
    }

    readMessage() {
        return this.messageQueue.unshift();
    }

    readAll() {
        let messagesCopy = this.messageQueue;
        this.messageQueue = [];
        return messagesCopy;
    }

    sendMessage(message, address) {
        this.server.send(message, address.port, address.address, (error) => {
            if(error){
                console.log("Error sending!", error);
            }
            else{

            }
        });
    }
};
