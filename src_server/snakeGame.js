const Config = require("../config/config");
const Field = require("./field");


module.exports = class SnakeGame {
    constructor(readMessagesCallback, sendMessageCallback) {
        this.readMessagesCallback = readMessagesCallback;
        this.sendMessageCallback = sendMessageCallback;
        this.field = new Field(Config.width, Config.height);
        this.frame = 0;
        this.snakeFrame = 0;
    }

    update() {
        this.frame += 1;

        let clientMessages = this.readMessagesCallback();
        for (let clientMessage of clientMessages) {
            this.executeClientMessage(clientMessage);
        }

        if (this.frame % Math.floor(Config.serverFps / Config.snakeFps) === 0){
            this.snakeFrame += 1;
            this.killInactivePlayers();
            this.field.update();
            this.sendClientMessages();
        }
    }

    killInactivePlayers() {
        for (let snake of this.field.snakes) {
            if (this.snakeFrame > snake.lastKeepaliveFrame + Config.maxFramesInactive) {
                console.log(`Killing inactive snake ${snake.name}`);
                this.field.removeSnake(snake);
            }
        }
    }

    executeClientMessage(clientMessage) {
        let foundSnake = false;
        for (let snake of this.field.snakes) {
            if (JSON.stringify(snake.socketAddress) === JSON.stringify(clientMessage.socket)) {
                this.processSnakeMessage(snake, clientMessage.message);
                foundSnake = true;
            }
        }

        if (!foundSnake) {
            this.addNewSnake(clientMessage);
        }
    }

    addNewSnake(clientMessage) {
        for (let snake of this.field.snakes) {
            if (snake.socketAddress.address === clientMessage.socket.address) {
                console.log(`Rejecting a snake that's to connect from the same IP: ${snake.socketAddress.address}`);
                return;
            }
            else if (snake.name === clientMessage.message.name) {
                console.log(`Rejecting snake, name already exists: ${snake.name}`);
                return;
            }
        }

        console.log(`Adding snake with name ${clientMessage.message.name}`);
        let newSnake = this.field.addSnake(clientMessage.socket, clientMessage.message.name);
        newSnake.lastKeepaliveFrame = this.snakeFrame;

        this.sendPlayerStatistics();
    }

    processSnakeMessage(snake, message) {
        if (message.type === "dir") {
            let direction = message.dir;
            if (direction === "l") {
                snake.leftPressed();
            }
            else if (direction === "r") {
                snake.rightPressed();
            }
            else if (direction === "u") {
                snake.upPressed();
            }
            else if (direction === "d") {
                snake.downPressed();
            }

            snake.lastKeepaliveFrame = this.snakeFrame;
        }
        else if (message.type === "keepalive") {
            snake.lastKeepaliveFrame = this.snakeFrame;
        }
    }

    sendClientMessages() {
        this.sendFieldToClients();
        if (this.snakeFrame % 4 === 0) {
            this.sendPlayerStatistics();
        }
    }

    sendFieldToClients() {
        let message = JSON.stringify(this.field.serialize());
        this.sendMessageToClients(message)
    }

    sendPlayerStatistics() {
        let message = {
            statistics: this.field.getSnakesStatistics(),
            type: "statistics"
        };

        let stringifiedMessage = JSON.stringify(message);
        this.sendMessageToClients(stringifiedMessage)
    }

    sendMessageToClients(message) {
        for (let snake of this.field.snakes) {
            this.sendMessageCallback(message, snake.socketAddress);
        }
    }
};
