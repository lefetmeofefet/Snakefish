const defaultConfig = require("./default");

try {
    console.log("Environment: ", process.env.NODE_ENV);
    const overrideConfig = require(`./${process.env.NODE_ENV}`);
    console.log("Override Config: ", overrideConfig);

    Object.assign(defaultConfig, overrideConfig);
}
catch (e) {
}

console.log("Config: ", defaultConfig);

module.exports = defaultConfig;
