
;;;;;;  ;;;;;;  ;;      ;; ;;             ;;;;;;;  ;;;;;;;  ;;       ;;;;;;;  ;;   ;;  ;;;;;
 ;;     ;;      ;;;;  ;;;;                ;;       ;;   ;;  ;;       ;;   ;;  ;;;  ;;  ;;
 ;;;;;  ;;;;;;  ;;  ;;  ;; ;;   ;;;;;;    ;;       ;;   ;;  ;;       ;;   ;;  ;; ; ;;  ;;;;;
    ;;  ;;      ;;      ;; ;;             ;;       ;;   ;;  ;;       ;;   ;;  ;;  ;;;     ;;
 ;;;;;  ;;;;;;  ;;      ;; ;;             ;;;;;;;  ;;;;;;;  ;;;;;;;  ;;;;;;;  ;;   ;;  ;;;;;

const Config = require("./config/config");
const UdpServer = require("./src_server/udpServer");
const SnakeGame = require("./src_server/snakeGame");

let FRAMES_TO_WAIT_FOR_STATISTICS = 60 * 5; // 5 seconds

let game;
let server;

function start() {
    server = new UdpServer();
    server.startListening();

    game = new SnakeGame(
        (...args) => server.readAll(...args),
        (...args) => server.sendMessage(...args)
    );

    setInterval(update, 1000.0 / Config.serverFps)
}


function update() {
    logFps();
    game.update();
}


let frames = 0;
let lastFramerateResetTime = Date.now();

function logFps() {
    frames += 1;
    if (frames === FRAMES_TO_WAIT_FOR_STATISTICS) {
        let timeNow = Date.now();
        let deltaTime = (timeNow - lastFramerateResetTime) / 1000.0;
        lastFramerateResetTime = timeNow;

        let fps = FRAMES_TO_WAIT_FOR_STATISTICS / deltaTime;
        console.log(`${FRAMES_TO_WAIT_FOR_STATISTICS} Frames in ${deltaTime}. FPS: ${fps}`);
        frames = 0;
    }
}

start();
