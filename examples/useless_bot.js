const Snakefish = require("snakefish")


let frame = 0;
console.log("ALL HAIL LORD SNAKEFISH");


/**
 * Runs each frame to determine snake action.
 * @param field Consists of all the data needed to understand the game state: width, height, apples, snakes and the controlled snake. example:
 *  {
 *      w: 20,
 *      h: 20,
 *      apples: [
 *          {x: 43, y: 21},
 *          { ... }
 *      ]
 *      snakes: [
 *          {
 *              head: {x: 13, y: 37},
 *              nodes: [
 *                  {x: 13, y: 37},
 *                  {x: 66, y: 6},
 *                  { ... }
 *              ],
 *              dir: 'u',
 *              color: 123123123,
 *              socketAddress: {
 *                  address: '192.968.1.90',
 *                  port: 1000000001
 *              }
 *          },
 *          { ... }
 *      ],
 *      mySnake: { ... }
 *  }
 * @returns 'r', 'l', 'u', 'd' to turn, or undefined - the snake will keep moving in the same direction.
 */
Snakefish.update = (field) => {
    console.log("Field: ", field)
    frame += 1;
    if (frame % 40 === 0) {
        return "r"
    }
    else if (frame % 40 === 10) {
        return "u"
    }
    else if (frame % 40 === 20) {
        return "l"
    }
    else if (frame % 40 === 30) {
        return "d"
    }
    else {
        // Not necessary, returning nothing returns undefined
        return undefined
    }
}
