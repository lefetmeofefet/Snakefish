const Snakefish = require("snakefish")

console.log("ALL HAIL LORD DAGNAHASH");

Snakefish.update = (field) => {
    try{
        let snake = field.mySnake;
        let love = field.apples[0];
        console.log(snake.dir)
        // If snake is moving horizontally
        if (snake.dir === 'r' || snake.dir === 'l') {
            if (snake.head.y !== love.y) {
                let verticalStraightDistance = snake.head.y - love.y;
                let verticalWallPassDistance = field.h - verticalStraightDistance;
                return verticalStraightDistance < verticalWallPassDistance ?
                    verticalStraightDistance > 0 ? 'u' : 'd'
                    :
                    verticalStraightDistance > 0 ? 'd' : 'u'
            }
        }
        else if (snake.dir === 'u' || snake.dir === 'd') {
            if (snake.head.x !== love.x) {
                let horizontalStraightDistance = snake.head.x - love.x;
                let horizontalWallPassDistance = field.w - horizontalStraightDistance;
                return horizontalStraightDistance < horizontalWallPassDistance ?
                    horizontalStraightDistance > 0 ? 'l' : 'r'
                    :
                    horizontalStraightDistance > 0 ? 'r' : 'l'
            }
        }
    }
    catch (e){
        console.log("Don't wanna die: ", e);
    }
}
