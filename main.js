const electron = require('electron');
const app = electron.app;
const dialog = require('electron').dialog;
const path = require('path');
const pjson = require('./package.json');
const isDev = require('electron-is-dev');

process.env.NODE_ENV = isDev ? "development" : "production";
const config = require("./config/config");

const _ = require('lodash');
const windowStateKeeper = require('electron-window-state');

const UdpClient = require("./src_client/node/udpClient");


// Manage unhandled exceptions as early as possible
process.on('uncaughtException', (e) => {
    console.error(`Caught unhandled exception: ${e}`);
    dialog.showErrorBox('Caught unhandled exception', e.message || 'Unknown error message');
    app.quit()
});


// This is exported when this is used as a library
exports.update = undefined;

// Define electron window objects
global.config = config;
global.getUserBotUpdateFunction = () => {
    return exports.update;
};
global.Client = new UdpClient();
global.SendMessage = function (message) {
    global.Client.sendMessage(message);
};

function sendMessageToWindow(message) {
    mainWindow.RunJavascript(`MessageReceived(${message})`)
}

if (isDev) {
    console.info('Running in development')
} else {
    console.info('Running in production')
}

// Adds debug features like hotkeys for triggering dev tools and reload
// (disabled in production, unless the menu item is displayed)
require('electron-debug')({
    enabled: isDev
});

// Prevent window being garbage collected
let mainWindow;

app.setName(pjson.name);

function createMainWindow() {
    // Load the previous window state with fallback to defaults
    let mainWindowState = windowStateKeeper({
        defaultWidth: 1024,
        defaultHeight: 768
    });

    const win = new electron.BrowserWindow({
        'width': mainWindowState.width,
        'height': mainWindowState.height,
        'x': mainWindowState.x,
        'y': mainWindowState.y,
        'title': app.getName(),
        'icon': path.join(__dirname, '/build/icon.ico'),
        'show': false, // Hide your application until your page has loaded
        'webPreferences': {
            'nodeIntegration': true
        }
    });

    // Let us register listeners on the window, so we can update the state
    // automatically (the listeners will be removed when the window is closed)
    // and restore the maximized or full screen state
    mainWindowState.manage(win);

    // Remove file:// if you need to load http URLs
    win.loadURL(`file://${__dirname}/${config.clientUrl}`, {});

    win.on('closed', () => {
        // Dereference used windows
        mainWindow = null
    });

    win.webContents.on('did-finish-load', () => {
        win.show();
        win.focus()
    });

    win.on('unresponsive', function () {
        console.warn('The windows is not responding')
    });

    win.webContents.on('did-fail-load', (error, errorCode, errorDescription) => {
        win.webContents.on('did-finish-load', () => {
            win.webContents.send('app-error', error + ", " + errorDescription)
        })
    });

    win.webContents.on('crashed', () => {
        console.error('The browser window has just crashed')
    });

    win.RunJavascript = function (code) {
        return win.webContents.executeJavaScript(code, true);
    };

    return win;
}

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', () => {
    if (!mainWindow) {
        mainWindow = createMainWindow()
    }
});

app.on('ready', () => {
    mainWindow = createMainWindow()
});

app.on('will-quit', () => {
    console.info("Closing!")
});

app.on('browser-window-created', (e, window) => {
    window.setMenu(null);
});