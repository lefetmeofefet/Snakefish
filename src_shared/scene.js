const _ = require('lodash');


module.exports = class Scene {
    constructor() {
        this.entities = this.initializeEntities();
        this.systems = this.initializeSystems();
    }

    initializeEntities() {
        throw "Method 'initializeEntities' is not implemented!"
    }

    initializeSystems() {
        throw "Method 'initializeSystems' is not implemented!"
    }

    update() {
        for (let system of this.systems) {
            system.update(this.entities.filter(entity => {
                let types = entity.components.map((component) => component.constructor);
                if (_.difference(system.getRequiredComponents(), types).length === 0) {
                    return true;
                }
            }));
        }
    }
}
