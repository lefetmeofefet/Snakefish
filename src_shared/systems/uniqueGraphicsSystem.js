const System = require("../system");
const UniqueGraphicsComponent = require("../components/uniqueGraphicsComponent");


module.exports = class UniqueGraphicsSystem extends System {
    constructor() {
        super([UniqueGraphicsComponent]);
    }

    update(entities) {
        for (let entity of entities) {
            entity[UniqueGraphicsComponent].render();
        }
    }
}
