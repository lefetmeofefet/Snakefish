const System = require("../system");
const UniqueBehaviorComponent = require("../components/uniqueBehaviorComponent");


module.exports = class UniqueBehaviorSystem extends System {
    constructor() {
        super([UniqueBehaviorComponent]);
    }

    update(entities) {
        for (let entity of entities) {
            entity[UniqueBehaviorComponent].update();
        }
    }
}
