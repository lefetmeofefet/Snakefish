const System = require("../system");
const RemoteControlledComponent = require("../components/input/remoteControlledComponent");
const SnakeInputComponent = require("../components/input/snakeInputComponent");


module.exports = class RemoteControlledSystem extends System {
    constructor(messageQueueCallback) {
        super([
            RemoteControlledComponent,
            SnakeInputComponent
        ]);

        this.getMessage = messageQueueCallback;
    }

    update(entities) {
        let message;
        while (true) {
            message = this.getMessage();
            if (message === undefined) {
                break;
            }

            for (let entity of entities) {
                if (message.address === entity[RemoteControlledComponent].address &&
                    message.port === entity[RemoteControlledComponent].port) {
                    this.processMessage(message.message, entity[SnakeInputComponent]);
                }
            }
        }
    }

    processMessage(message, snakeInputComponent) {
        if (message === "r") {
            snakeInputComponent.inputDirection = 'r';
        }
        else if (message === "l") {
            snakeInputComponent.inputDirection = 'l';
        }
        else if (message === "u") {
            snakeInputComponent.inputDirection = 'u';
        }
        else if (message === "d") {
            snakeInputComponent.inputDirection = 'd';
        }
    }
}
