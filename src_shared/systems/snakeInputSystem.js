const System = require("../system");
const SnakeInputComponent = require("../components/input/snakeInputComponent");
const SnakeComponent = require("../components/snake/snakeComponent");


module.exports = class SnakeInputSystem extends System {
    constructor() {
        super([
            SnakeInputComponent,
            SnakeComponent
        ]);
    }

    update(entities) {
        for (let entity of entities) {
            entity[SnakeComponent].direction = entity[SnakeInputComponent].direction;
        }
    }
}
