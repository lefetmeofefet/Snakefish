const Scene = require('../scene');
const FieldEntity = require('../entities/fieldEntity');
const UniqueBehaviorSystem = require('../systems/uniqueBehaviorSystem');
const UniqueGraphicsSystem = require('../systems/uniqueGraphicsSystem');


module.exports = class GameScene extends Scene {
    constructor(server) {
        super();
        this.server = server;
    }

    initializeEntities() {
        this.fieldEntity = new FieldEntity();

        return [
            this.fieldEntity
        ]
    }

    initializeSystems() {
        this.uniqueBehaviorSystem = new UniqueBehaviorSystem()
        this.uniqueGraphicsSystem = new UniqueGraphicsSystem()

        return [
            this.uniqueBehaviorSystem,
            this.uniqueGraphicsSystem
        ]
    }
}
