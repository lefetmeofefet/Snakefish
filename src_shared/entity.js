module.exports = class Entity {
    constructor(components = []) {
        this.components = components;
        for (let component of components) {
            this[component.constructor] = component;
        }
    }

    addComponent(component) {
        this.components.push(component);
        this[component.constructor] = component;
    }

    removeComponent(component) {
        let index = this.components.indexOf(component);
        if (index > -1) {
            let removedComponent = this.components.splice(index);
            delete this[removedComponent];
        }
    }
}
