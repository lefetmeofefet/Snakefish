const Entity = require("../entity");
const UniqueBehaviorComponent = require("../components/uniqueBehaviorComponent");
const UniqueGraphicsComponent = require("../components/uniqueGraphicsComponent");
const GridComponent = require("../components/physics/gridComponent");


module.exports = class FieldEntity extends Entity {
    constructor(width = 10, height = 10) {
        super([
            new GridComponent(width, height),
            new UniqueBehaviorComponent(() => this.update()),
            new UniqueGraphicsComponent(() => this.render())
        ]);
    }

    update() {
        console.log("Updated")
    }

    render() {
        console.log("Rendereded")
    }
}
