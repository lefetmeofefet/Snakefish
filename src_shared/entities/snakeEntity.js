const Entity = require("../entity");
const UniqueBehaviorComponent = require("../components/uniqueBehaviorComponent");
const UniqueGraphicsComponent = require("../components/uniqueGraphicsComponent");
const SnakeComponent = require("../components/snake/snakeComponent");


module.exports = class SnakeEntity extends Entity {
    constructor(fieldComponent) {
        super([
            new UniqueBehaviorComponent(() => this.update()),
            new UniqueGraphicsComponent(() => this.render()),
            new SnakeComponent()
        ]);
        this.fieldComponent = fieldComponent;
    }

    update() {
        let snake = this[SnakeComponent];

        let newHead;
        let currentHead = snake.head;

        if (snake.direction === "l"){
            newHead = {
                x: currentHead.x - 1,
                y: currentHead.y
            }
        }
        else if (snake.direction === "r"){
            newHead = {
                x: currentHead.x + 1,
                y: currentHead.y
            }
        }
        else if (snake.direction === "u"){
            newHead = {
                x: currentHead.x,
                y: currentHead.y - 1
            }
        }
        else if (snake.direction === "d"){
            newHead = {
                x: currentHead.x,
                y: currentHead.y + 1
            }
        }

        if (newHead.x > this.fieldComponent.width - 1){
            newHead.x = 0
        }
        if (newHead.y > this.fieldComponent.height - 1){
            newHead.y = 0
        }
        if (newHead.x < 0){
            newHead.x = this.fieldComponent.width - 1
        }
        if (newHead.y < 0){
            newHead.y = this.fieldComponent.height - 1
        }

        snake.body.push(newHead);

        // if (!this.ateApple){
        //     this.nodes.splice(0, 1);
        // }
        // else{
        //     this.ateApple = false;
        // }
    }

    render() {
        console.log("Rendereded")
    }
}
