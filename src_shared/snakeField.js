class SnakeField {
    constructor(width, height) {
        this.width = width;
        this.height = height;

        this.snakes = [];
        this.apples = []
    }

    addSnake(snake) {
        this.snakes.push(snake);
        this.addChild(snake);
    }

    addRandomSnake(color) {
        if (color === undefined) {
            color = Utils.getRandomInt(0, 0xffffff);
        }
        let snake = new Snake(
            Utils.getRandomInt(0, this.width - 1),
            Utils.getRandomInt(0, this.width - 1),
            color
        );

        let dir;
        switch (Utils.getRandomInt(0, 3)) {
            case 0:
                dir = "l";
                break;
            case 1:
                dir = "r";
                break;
            case 2:
                dir = "u";
                break;
            case 3:
                dir = "d";
                break;
        }
        snake.dir = dir;

        this.addSnake(snake);
        return snake;
    }

    removeSnake(snake) {
        snake.destroy();
        let i = this.snakes.indexOf(snake);
        if (i !== -1) {
            this.snakes.splice(i, 1);
        }
    }

    killSnake(snake) {
        this.removeSnake(snake);

        let newSnake = this.addRandomSnake(snake.color);
        if (snake === this.mySnake) {
            this.mySnake = newSnake;
        }
    }

    createApple() {
        this.apples.push({
            x: Utils.getRandomInt(0, this.width - 1),
            y: Utils.getRandomInt(0, this.height - 1)
        });
        this.renderApples();
    }
}