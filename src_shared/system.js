module.exports = class System {
    constructor(requiredComponents) {
        this.requiredComponents = requiredComponents;
    }

    getRequiredComponents() {
        return this.requiredComponents;
    }

    update(entities) {
        throw "Update method of system must be implemented";
    }
}