const Component = require('../component');


module.exports = class UniqueGraphicsComponent extends Component {
    constructor(renderFunction) {
        super();
        this.renderFunction = renderFunction;
    }

    render() {
        this.renderFunction();
    }
}
