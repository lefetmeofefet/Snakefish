const Component = require('../component');


module.exports = class UniqueBehaviorComponent extends Component {
    constructor(updateFunction) {
        super();
        this.updateFunction = updateFunction;
    }

    update() {
        this.updateFunction();
    }
}
