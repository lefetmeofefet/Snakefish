const Component = require("../../component");


module.exports = class RemoteControlledComponent extends Component {
    constructor(address, port) {
        super();
        this.address = address;
        this.port = port;
    }
}
