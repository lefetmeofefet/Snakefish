const Component = require("../../component");


module.exports = class positionComponent extends Component {
    constructor(x = 0, y = 0) {
        super();
        this.x = x;
        this.y = y;
    }
}

