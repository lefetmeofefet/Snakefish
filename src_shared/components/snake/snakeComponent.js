const Component = require("../../component");
const PositionComponent = require("../physics/positionComponent");


module.exports = class snakeComponent extends Component {
    get head() {
        return this.body[this.body.length - 1];
    }

    constructor(x = 0, y = 0, direction= 'r') {
        super();
        this.body = [
            new PositionComponent(x, y)
        ];
        this.direction = direction;
    }
}
