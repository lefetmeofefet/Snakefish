const dgram = require("dgram");
const config = require("../../config/config");
const os = require("os");

module.exports = class UdpClient {
    static get PORT() {
        return config.port;
    }

    static get HOST() {
        return config.address;
    }

    constructor(host = UdpClient.HOST, port = UdpClient.PORT) {
        this.client = dgram.createSocket('udp4');
        this.messageQueue = [];
        this.port = port;
        this.host = host;
        this.authenticated = false;

        this.client.on("message", (message, remote) => {
            if (this.authenticated) {
                this.messageQueue.push(message);
            }
            else{
                message = JSON.parse(message);
                console.log("Authenticating challenge: ", message);

                // Send the challenge string back to server, to prove that the IP is not fabricated
                this.authenticate(message.challenge);
            }
        });

        this.sendAuthenticationRequest();
    }

    sendAuthenticationRequest() {
        this.sendMessage(JSON.stringify({
            type: "auth"
        }));
    }

    authenticate(challengeString) {
        this.sendMessage(JSON.stringify({
            type: "challenge",
            challenge: challengeString,
            name: os.hostname()
        }));
        this.authenticated = true;
    }

    readMessage() {
        return this.messageQueue.unshift();
    }

    readAll() {
        let messagesCopy = this.messageQueue;
        this.messageQueue = [];
        return messagesCopy;
    }

    sendMessage(message) {
        this.client.send(new Buffer(message), 0, message.length, this.port, this.host, (err, bytes) => {
            if (err) {
                this.client.close();
                throw err;
            }
        });
    }
};
