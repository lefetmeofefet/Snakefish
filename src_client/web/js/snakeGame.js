const remote = require('electron').remote;


let stage;

let field;
let scoreBoard;

let Frame = 0;

function main() {
    stage = new PixiGame(0x272727);
    setup();
}

function setup() {
    field = new Field();
    scoreBoard = new ScoreBoard();

    stage.addChild(field);
    stage.addChild(scoreBoard);
    stage.startGame(update);
}

function update() {
    Frame += 1;

    for (let message of Communicator.readServerMessages()) {
        let parsedMessage = JSON.parse(message);

        if (parsedMessage.type === "field") {
            loadField(parsedMessage);
        }
        else if (parsedMessage.type === "statistics") {
            scoreBoard.loadSerializedPlayerStatistics(parsedMessage.statistics);
        }
    }
}

function loadField(fieldMessage) {
    field.loadSerializedField(fieldMessage);

    let botResponse = Communicator.runBot(fieldMessage);
    if (botResponse) {
        Communicator.sendMessage(JSON.stringify(botResponse));
    }
    else {
        Communicator.sendMessage(JSON.stringify({
            type: 'keepalive'
        }));
    }
}

main();
