let Utils = {
    createKey: function (keyCode, onPress, onRelease) {
        let key = {};
        key.code = keyCode;
        key.isDown = false;
        key.isUp = true;
        key.onPress = onPress;
        key.onRelease = onRelease;

        key.downHandler = function (event) {
            if (event.keyCode === key.code) {
                if (key.isUp && key.onPress) key.onPress();
                key.isDown = true;
                key.isUp = false;
            }
            event.preventDefault();
        };

        key.upHandler = function (event) {
            if (event.keyCode === key.code) {
                if (key.isDown && key.onRelease) {
                    key.onRelease();
                }
                key.isDown = false;
                key.isUp = true;
            }
            event.preventDefault();
        };

        //Attach event listeners
        window.addEventListener(
            "keydown", key.downHandler.bind(key), false
        );
        window.addEventListener(
            "keyup", key.upHandler.bind(key), false
        );
        return key;
    },
    getRandomInt: function (min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    }
};
