class ScoreBoard extends PIXI.Container {
    constructor() {
        super();

        this.scoresGraphics = new PIXI.Graphics();

        let style = new PIXI.TextStyle({
            fontFamily: 'Arial',
            fontSize: 20
        });

        this.text = new PIXI.Text('SCORES:', style);
        this.text.x = 1.025;
        this.text.y = 0.025;
        this.text.scale.set(0.002, 0.002);

        this.addChild(this.text);
        this.addChild(this.scoresGraphics);
        this.renderScores();
    }

    loadSerializedPlayerStatistics(playersStatistics) {
        this.text.text = "SCORES:";
        for (let snakeStats of playersStatistics) {
            this.text.text += `\n${snakeStats.name} \n\t  max-length: ${snakeStats.maxLength}\n\t  score: ${snakeStats.score}\n\t  deaths: ${snakeStats.deaths}`
        }
    }

    renderScores() {
        this.scoresGraphics.clear();
        this.scoresGraphics.beginFill(0xffffff, 0.1);

        this.scoresGraphics.drawRect(1.005, 0.005, 1, 0.995);
        this.scoresGraphics.endFill();
    }
}