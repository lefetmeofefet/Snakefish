class Field extends PIXI.Container {
    get width() {
        return this._width;
    }
    set width(val){
        this._width = val;
    }

    get height() {
        return this._height;
    }
    set height(val){
        this._height = val;
    }

    constructor() {
        super();

        this.width = 1;
        this.height = 1;

        this.snakes = [];
        this.apples = [];

        this.applesGraphics = new PIXI.Graphics();
        this.gridGraphics = new PIXI.Graphics();

        this.drawGrid();

        this.addChild(this.applesGraphics);
        this.renderApples();

        Utils.createKey(37).onPress = () => {
            Communicator.sendMessage(JSON.stringify({
                type: 'dir',
                dir: 'l'
            }));
        };
        Utils.createKey(38).onPress = () => {
            Communicator.sendMessage(JSON.stringify({
                type: 'dir',
                dir: 'u'
            }));
        };
        Utils.createKey(39).onPress = () => {
            Communicator.sendMessage(JSON.stringify({
                type: 'dir',
                dir: 'r'
            }));
        };
        Utils.createKey(40).onPress = () => {
            Communicator.sendMessage(JSON.stringify({
                type: 'dir',
                dir: 'd'
            }));
        };
    }

    loadSerializedField(field) {
        this.apples = field.apples;

        for (let currentSnake of this.snakes) {
            currentSnake.destroy();
        }
        this.snakes = [];

        for (let snake of field.snakes) {
            let newSnake = new Snake(snake.color, this);
            newSnake.nodes = snake.nodes;
            this.snakes.push(newSnake);
            this.addChild(newSnake);
        }

        if (this.width !== field.w || this.height !== field.h) {
            this.width = field.w;
            this.height = field.h;
            this.drawGrid();
        }

        this.renderApples();
        this.renderSnakes();
    }

    drawGrid() {
        this.gridGraphics.clear();

        let fieldLineWidth = Config.fieldLineWidth / Math.max(this.width, this.height);
        this.gridGraphics.lineStyle(fieldLineWidth, 0x1d1d1d, 1);

        // Draw grid lines
        for (let i=0; i<this.width; i++) {
            this.gridGraphics.moveTo(i / this.width, 0);
            this.gridGraphics.lineTo(i / this.width, 1);
        }

        for (let i=0; i<this.height; i++) {
            this.gridGraphics.moveTo(0, i / this.height);
            this.gridGraphics.lineTo(1, i / this.height);
        }

        // Draw border
        this.gridGraphics.moveTo(1, 0);
        this.gridGraphics.lineTo(1, 1);

        this.gridGraphics.moveTo(0, 1);
        this.gridGraphics.lineTo(1, 1);

        this.addChild(this.gridGraphics);
    }

    renderApples() {
        this.applesGraphics.clear();
        this.applesGraphics.beginFill(Config.appleColor, 1);

        let fieldLineWidth = Config.fieldLineWidth / Math.max(this.width, this.height);
        for (let apple of this.apples){
            let position = this.getRectOfTile(apple.x, apple.y, fieldLineWidth);
            this.applesGraphics.drawRect(position.x, position.y, position.w, position.h);
        }
        this.applesGraphics.endFill();
    }

    renderSnakes() {
        for (let snake of this.snakes) {
            snake.render();
        }
    }

    getRectOfTile(x, y, fieldLineWidth = 0){
        return {
            x: x / this.width + fieldLineWidth,
            y: y / this.height + fieldLineWidth,
            w: 1 / this.width - 2 * fieldLineWidth,
            h: 1 / this.height - 2 * fieldLineWidth
        }
    }
}