class Snake extends PIXI.Graphics{
    constructor(color, field) {
        super();

        this.field = field;
        this.color = color;
        this.nodes = [];
        this.render();
    }

    render() {
        this.clear();
        let fieldLineWidth = Config.fieldLineWidth / Math.max(this.field.width, this.field.height);

        // Rectangles
        this.beginFill(this.color, 1);
        for (let node of this.nodes){
            let position = this.field.getRectOfTile(node.x, node.y, fieldLineWidth);
            this.drawRect(position.x, position.y, position.w, position.h);
        }
        this.endFill();

        // Rectangle borders
        this.lineStyle(fieldLineWidth, 0x0f0f0f, 1);
        for (let node of this.nodes){
            let position = this.field.getRectOfTile(node.x, node.y, fieldLineWidth);
            position.x -= fieldLineWidth / 2;
            position.y -= fieldLineWidth / 2;
            position.w += fieldLineWidth;
            position.h += fieldLineWidth;

            this.moveTo(position.x, position.y);
            this.lineTo(position.x + position.w, position.y);

            this.moveTo(position.x + position.w, position.y);
            this.lineTo(position.x + position.w, position.y + position.h);

            this.moveTo(position.x + position.w, position.y + position.h);
            this.lineTo(position.x, position.y + position.h);

            this.moveTo(position.x, position.y + position.h);
            this.lineTo(position.x, position.y);

            this.drawRect(position.x, position.y, position.w, position.h);
        }
    }
}