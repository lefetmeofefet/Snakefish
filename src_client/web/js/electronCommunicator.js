let os = require('os');


let Communicator = (() => {

    function getMySnake(field) {
        let localAddress = this.getLocalSocketAddress();
        for (let snake of field.snakes) {
            if (snake.socketAddress.address === localAddress &&
                snake.socketAddress.port === remote.getGlobal('Client').client.address.port()) {
                return snake;
            }
        }
    }

    function getLocalSocketAddress() {
        let interfaces = os.networkInterfaces();
        for (let k in interfaces) {
            for (let k2 in interfaces[k]) {
                let address = interfaces[k][k2];
                if (address.family === 'IPv4' && !address.internal) {
                    return address.address;
                }
            }
        }
    }

    return {
        readServerMessages: function () {
            return remote.getGlobal('Client').readAll();
        },

        sendMessage: function (message) {
            remote.getGlobal("SendMessage")(message);
        },

        runBot: function (field) {
            let botFunction = remote.getGlobal("getUserBotUpdateFunction")();
            if (!botFunction) {
                return undefined
            }

            field.mySnake = this.getMySnake(field);
            let botResponse = botFunction(field);
            if (botResponse === undefined) {
                return botResponse;
            }
            else if (['r', 'l', 'u', 'd'].some((dir) => dir === botResponse)) {
                return {
                    type: 'dir',
                    dir: botResponse
                }
            }
            else {
                throw "Output value must be one of the following: 'r', 'l', 'u', 'd', and undefined if no action is taken"
            }
        },

        messageReceived: function (message) {
            console.log("Message received", message);
        }
    }
})();
