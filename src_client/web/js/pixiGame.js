class PixiGame extends PIXI.Container {
    constructor(backgroundColor) {
        super();

        this.renderer = PIXI.autoDetectRenderer(window.innerWidth, window.innerHeight, {
            antialias: true,
            transparent: false,
            resolution: 1
        });
        this.renderer.view.style.position = "absolute";
        this.renderer.view.style.display = "block";
        this.renderer.autoResize = true;

        this.resize();
        window.addEventListener('resize', () => this.resize(), false);

        if (backgroundColor) {
            this.renderer.backgroundColor = backgroundColor;
        }

        document.body.appendChild(this.renderer.view);
    }

    resize() {
        this.renderer.resize(window.innerWidth, window.innerHeight);

        let w = this.renderer.width,
            h = this.renderer.height;
        let scale = Math.min(w * 0.7, h);
        this.scale.set(scale, scale);
    }

    startGame(updateCallback) {
        this.updateCallback = updateCallback;
        this.gameLoop();
    }

    gameLoop() {
        requestAnimationFrame(() => this.gameLoop());
        this.updateCallback();
        this.renderer.render(this);
    }
}